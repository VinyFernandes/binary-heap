/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vinicius
 *
 * Created on 11 de Maio de 2018, 18:15
 */

#include <iostream>
#include <cstdlib>

#include "BinaryHeap.h"

using namespace std;

/**
 * @return which function of the Binary Heap the user wants to call.
 */
int menu(); //prototype menu function

/*
 * Main function to test the class BInary Heap
 */
int main(int argc, char** argv) {

    BinaryHeap* heap = NULL;
    int *vector = NULL;
    int option;
    int key;
    int size;

    system("clear");
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐             WELCOME             ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " Enter the initial capacity of the heap: ";
    cin >> size;

    if (size < 0) {
        cout << "\n ERROR: The initial capacity of heap should be positive." << endl;
        exit(-1);
    }

    heap = new BinaryHeap(size); // Initialize Heap.
        heap->Insert(95);
        heap->Insert(73);
        heap->Insert(78);
        heap->Insert(33);
        heap->Insert(60);
        heap->Insert(66);
        heap->Insert(70);
        heap->Insert(20);
        heap->Insert(25);
        heap->Insert(28);

    do {
        option = menu();
        system("clear");

        switch (option) {

            case 1:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              SIZE               ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " Size: " << heap->Size() << endl;
                break;

            case 2:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐           HEAP CAPACITY         ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " HeapCapacity: " << heap->HeapCapacity() << endl;
                break;

            case 3:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              SELECT             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                key = heap->Select();
                if (heap->Select() != -1)
                    cout << " Select: " << key << endl;
                else
                    cout << " Select: Empty Heap" << endl;
                break;

            case 4:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              REMOVE             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                key = heap->Remove();
                if (key != -1)
                    cout << " Remove: " << "The node " << key << " was successfully removed." << endl;
                else
                    cout << " Remove: Empty Heap" << endl;
                break;

            case 5:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              INSERT             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " Enter the key: ";
                cin >> key;
                if (heap->Insert(key))
                    cout << "\n Insert: " << "Key " << key << " inserted successfully." << endl;
                else
                    cout << "\n Insert: Overflow: Could not insert Key." << endl;
                break;

            case 6:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              SEARCH             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " Enter the key: ";
                cin >> key;

                if (heap->Search(key) != -1)
                    cout << "\n Search: " << "key " << key << " found with success." << endl;
                else
                    cout << "\n Search: The key does not exist in the heap." << endl;
                break;

            case 7:
                int new_key;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              UPDATE             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " Enter the KEY of the node you want to update: ";
                cin >> key;
                cout << " Enter the new node key: ";
                cin >> new_key;

                if (heap->Update(key, new_key)) {
                    cout << "\n Update: " << "key updated successfully. ";
                    cout << "(" << key << " -> " << new_key << ")" << endl;
                } else
                    cout << "\n Update: key update failed." << endl;
                break;

            case 8:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              HEAPIFY            ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " Enter the size of the vector: ";
                cin >> size;
                vector = new int[size];

                if (size < (heap->HeapCapacity() - heap->Size())) {

                    cout << " Enter the (" << size << ") keys of the vector.\n" << endl;
                    for (int i = 0; i < size; i++) {
                        cout << " N" << i + 1 << ": ";
                        cin >> key;
                        vector[i] = key;
                    }
                    heap->Heapify(vector, size);
                    cout << "\n Heapify: Vector reorganized in heap with success." << endl;
                } else
                    cout << "\n Heapify: Overflow: Could not insert vector." << endl;

                delete vector;
                break;

            case 9:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐            PRINT HEAP           ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " PrintHeap: ";
                heap->PrintHeap();
                cout << endl;
                break;

            case 10:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐       BINARY TREE TO HEAP       ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                if (heap->GetTree() != NULL) {
                    BinaryHeap* heapConversion = new BinaryHeap(heap->HeapCapacity());

                    heapConversion->BinaryTreeToHeap(heap->GetTree()->GetRoot());

                    cout << "\n Tree: ";
                    cout << "[";
                    heap->GetTree()->PreOrder(heap->GetTree()->GetRoot());
                    cout << "]";

                    cout << "\n\n Heap: ";
                    heapConversion->PrintHeap();
                    cout << endl;
                } else
                    cout << "\n BinaryTreeToHeap: The binary tree can not be empty." << endl;
                break;

            case 11:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐       HEAP TO BINARY TREE       ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                if (heap->Size() > 0) {
                    BinaryTree* tree = heap->HeapToBinaryTree();

                    cout << "\n Heap: ";
                    heap->PrintHeap();
                    
                    cout << "\n\n Tree: [";
                    tree->PreOrder(tree->GetRoot());
                    cout << "]" << endl;
                } else
                    cout << "\n HeapToBinaryTree: Empty Heap";
                break;

            case 12:
                int capacity;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐          CHANCE CAPACITY        ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " Enter the new capacity of the Heap: ";
                cin >> capacity;
                
                if (heap->ChangeCapacity(capacity))
                    cout << "\n ChanceCapacity: " << "Capacity changed successfully." << endl;
                else
                    cout << "\n ChanceCapacity: Failure to change capacity." << endl;
                break;

            case 0:
                exit(0);
                break;

            default:
                cout << " \n Invalid option." << endl;
                break;
        }

        cout << "\n\n Do you want to continue to carry out operations at BinayHeap? " << endl;
        cout << " ( 1 - YES / 2 - NOT ):  ";
        cin >> option;

        if (option == 2)
            exit(0);

    } while (true);

    return 0;
}

/**
 * @return which function of the Binary Heap the user wants to call.
 */
int menu() {
    int option = 0;

    system("clear");
    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                           BINARY HEAP                          ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << "▐\t" << "[ 1 ]  *Size                 [ 7 ]   *Update             ▐" << endl;
    cout << "▐\t" << "[ 2 ]  *HeapCapacity         [ 8 ]   *Heapify            ▐" << endl;
    cout << "▐\t" << "[ 3 ]  *Select               [ 9 ]   *PrintHeap          ▐" << endl;
    cout << "▐\t" << "[ 4 ]  *Remove               [ 10 ]   BinaryTreeToHeap   ▐" << endl;
    cout << "▐\t" << "[ 5 ]  *Insert               [ 11 ]  *HeapToBinaryTree   ▐" << endl;
    cout << "▐\t" << "[ 6 ]  *Search               [ 12 ]  *ChangeCapacity     ▐" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " \n What do you want to do?  ";
    cin >> option;

    return option;
}


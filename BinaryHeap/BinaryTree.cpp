/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryTree.cpp
 * Author: vinicius
 * 
 * Created on 14 de Maio de 2018, 19:18
 */

#include "BinaryTree.h"

/**
 * Constructor: Builds a binary tree. root arrow as null.
 */
BinaryTree::BinaryTree() {
    this->root = NULL;
}

/**
 * Constructor: Builds a binary tree
 * 
 * @param key to set
 */
BinaryTree::BinaryTree(int key) {
    this->root = new NodeTree(key);
}

BinaryTree::BinaryTree(const BinaryTree& orig) {
}

/**
 * Destroyer binary tree. Automatically clears the allocated memory space
 * for the binary tree.
 */
BinaryTree::~BinaryTree() {
    delete this->root;
}

/**
 * root node of the tree to set.
 * 
 * @param root node. 
 */
void BinaryTree::SetRoot(NodeTree* root) {
    this->root = root;
}

/**
 * @return current root node of the tree.
 */
NodeTree* BinaryTree::GetRoot() const {
    return this->root;
}

/**
 * @return true if the tree is empty (root = NULL)
 * or false if the tree has nodes (root != NULL).
 */
bool BinaryTree::Empty() {
    return (this->root == NULL);
}

/**
 * 
 * 
 * @param key
 * @param ptRoot
 * 
 * @return 
 */
int BinaryTree::Search(int key, NodeTree*& ptRoot) {

    int f; // indicates search result: 0, 1, 2 or 3.

    if (ptRoot == NULL)
        f = 0; // Empty tree.
    else if (key == ptRoot->GetKey())
        f = 1; // found!
    else if (key < ptRoot->GetKey()) { // Search left side.

        if (ptRoot->GetLeft() == NULL)
            f = 2;
        else {
            ptRoot = ptRoot->GetLeft(); // updating pointer: new root!
            f = this->Search(key, ptRoot);
        }

    } else { // Search right side.

        if (ptRoot->GetRight() == NULL)
            f = 3;
        else {
            ptRoot = ptRoot->GetRight(); // updating pointer: new root!
            f = this->Search(key, ptRoot);
        }

    }
    return f; // search result.
}

/**
 * 
 * 
 * @param key
 * @param ptRoot
 * @return 
 */
bool BinaryTree::Insert(int key, NodeTree*& ptRoot) {

    int f;
    NodeTree* pt;
    bool insOK = true;
    NodeTree* ptAux = ptRoot; // Save root, ptAx will be modified by Search().

    f = this->Search(key, ptAux); // ptAux points to the root of the tree.

    if (f == 1) // Already in the tree
        insOK = false;
    else { // Deciding to insert into the subtree: left ou right

        pt = new NodeTree(key); // Allocate new tree node

        if (f == 0)
            this->root = pt; // new root.
        else if (f == 2) // insert to left
            ptAux->SetLeft(pt);
        else // f = 3
            ptAux->SetRight(pt);
    }
    return insOK;
}

/**
 * runs the tree in preorder, started from the root.
 * 
 * @param tree root.
 */
void BinaryTree::PreOrder(NodeTree* root) {

    if (!this->Empty()) {
        this->Visit(root);

        if (root->GetLeft() != NULL)
            this->PreOrder(root->GetLeft());

        if (root->GetRight() != NULL)
            this->PreOrder(root->GetRight());
    }
}

void BinaryTree::InOrder(NodeTree* root) {

    if (root->GetLeft() != NULL)
        InOrder(root->GetLeft());

    Visit(root);

    if (root->GetRight() != NULL)
        InOrder(root->GetRight());
}

/**
 * visit the node passed by parameter, displaying its key.
 * 
 * @param tree root.
 */
void BinaryTree::Visit(NodeTree* root) {
    cout << root->GetKey() << ", ";
}

